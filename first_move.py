import gpiozero
import time
from acc_dec_profiles import accdec

robot = gpiozero.Robot(left=(17,18), right=(27,22))

def get_move_profile():
  return accdec.linear(
      speed=1.0, acc_time=.4, dec_time=.4, total_time=1.0, freq=30.0)

def get_turn_profile():
  return accdec.linear(
      speed=.80, acc_time=.25, dec_time=.25, total_time=0.5, freq=30.0)

def move_or_turn(movement, profile):
  for duration, speed in profile:
    movement(speed)
    time.sleep(duration)

def move(movement):
  move_or_turn(movement, get_move_profile())

def turn(movement):
  move_or_turn(movement, get_turn_profile())

for i in range(4):
  print('iteration %d' % i)
  move(robot.forward)
  turn(robot.right)
#  move(lambda speed: print('forward %f' % speed))
#  turn(lambda speed: print('right %f' % speed))
